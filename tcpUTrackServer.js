
var net = require('net');
var request = require('request');
var moment = require('moment');
var mongojs = require('mongojs');
var locations = mongojs('mongodb://localhost:27017/geonames', ['address']);
var db = mongojs('127.0.0.1:27017/gps_server', ['routes']);
var geolib = require('geolib');
var cameraUrl = "http://fourzip.com/g/";

var toJsonPacket = function(packetString, cb) {
    try {
        var data = packetString.toString();
        dataArray = data.split(',');
        var time = dataArray[18];
        var dd = dataArray[19].substring(0, 2);
        var mm = dataArray[19].substring(2, 4);
        var yy = dataArray[19].substring(4, 6);
        var tstamp = yy + mm + dd + time;
        var deviceDate = timeStampToDate(tstamp);
        var timeStamp = addMinutes(deviceDate, 330);
        // var recivedJson = JSON.parse(packetString);
        var packet = {
            messageStart: dataArray[0],
            // message start
            serial_number: dataArray[1],
            // uid of the device
            messageSerial: dataArray[2],
            // message serial_number
            reason: dataArray[3],

            cmd_key: dataArray[4],
            // use to check event condition
            /***********************************************/
            /**/
            command_value: dataArray[5],
            ignition: dataArray[6],
            power_status: dataArray[7],
            box_open_status: dataArray[8],
            messageKey: dataArray[9],
            odometer: dataArray[10],
            speed: parseInt(dataArray[11]),
            no_of_satellites: dataArray[12],
            packet_validity: dataArray[13],
            latitude: dataArray[14],
            longitude: dataArray[15],
            altitude: dataArray[16],
            direction: dataArray[17],
            timestamp: timeStamp,
            gsm_signal: dataArray[20],
            gsm_register: dataArray[21],
            gprs: dataArray[22],
            internal_battery: dataArray[24],
            external_battery: dataArray[25],
            digital_io: dataArray[26],
            analog_1: dataArray[27], // for fuel.
            analog_2: parseFloat(parseFloat(dataArray[28]) / 100), // for temprature.
            analog_3: dataArray[29],
            analog_4: dataArray[30],
            hardware_version: dataArray[31],
            software_version: dataArray[32],
            packet_status: function packetStatusChecker(status) {
                if (status == "0") {
                    return "online";
                } else {
                    return "offline";
                }
            }(((dataArray[33]).toString()).substring(0, 1)),

        }
        return cb(null,packet);
    } catch (e) {
      return  cb(e);
    }

}



// gpspacket.data={};
// gpspacket.data.includes= {};
// gpspacket.data.includes.alerts = [];
// gpspacket.data.relationships = {};
var packet, geoFence = {};
net.createServer(function(socket) {


    socket.on('connection', function() {
        console.log('socket connected');
    });
    socket.on('data', function(data, remote) {
        category = "NA";

       toJsonPacket(data,function(err,packet){
         if(err){
           console.log("Got error during packets formation",err);
         }else{
           // after packet created succesfully
           var implementCode = function() {

               if (geoFence.hasOwnProperty(packet.serial_number)) {
                   if (packet.packet_validity == "A") {
                       console.log("valid");
                       //wstream.write(line+'\n');
                       createPacketTOSend(packet, function(gpspacket) {
                           checkAlertsInPacket(packet, function(data) {

                               if (data) {
                                  //  console.log(data);
                                   gpspacket.data.includes.alerts.push(data);
                               }
                               /*******  memory leaking in below block *********/
                               getGeofenceAlerts(packet, geoFence, function(data) {
                                   if (data) {
                                       gpspacket.data.includes.alerts.push(data);
                                   }
                                   setAddress(gpspacket, function(gpspacket) {
                                      /**** below request is also causing memory leak ***/
                                       request.post({
                                           url: 'http://127.0.0.1:64444/api/v1/packets',
                                           form: gpspacket
                                       }, function(err, httpResponse, body) {
                                          //  console.log(body);
                                       });
                                   });
                               });
                           });
                       });




                   } else {
                       console.log("Invalid");
                   }
               } else {
                   geoFence[packet.serial_number] = {
                       "locLat": 0,
                       "locLng": 0,
                       "status": false,
                       "current_location_id": "default"

                   }
                   implementCode();
               }
           }
           implementCode();
         }
        });


    });
    socket.on('end', function() {
        console.log('end');
    });
    socket.on('close', function() {
        console.log('close');
    });
    socket.on('error', function(e) {
        console.log('error ', e);
    });
    socket.write('hello from tcp server');
}).listen(17212, function() {
    console.log('TCP Server is listening on port 17212');

});


function checkAlertsInPacket(packets, cb) {
    var packetAlert = {
        tStamp: packets.timestamp,
        latitude: packets.latitude,
        longitude: packets.longitude,
        serial_number: packets.serial_number
    };
    switch (packets.reason) {

        case "V":
            packetAlert.severity = "normal";
            packetAlert.data = cameraUrl + packets.command_value;
            packetAlert.category = "Camera";
            packetAlert.type = "Snapshot";
            packetAlert.description = "The camera has captured a new picture.";
            packetAlert.show = false;
            break;

        case "C":
            packetAlert.severity = "normal";
            packetAlert.data = packets.speed;
            packetAlert.category = "Speed";
            packetAlert.type = "Normal speed";
            packetAlert.description = "Vehicle back to normal speed";
            packetAlert.show = false;
            break;

        case "D":
            packetAlert.severity = "crucial";
            packetAlert.type = "Overspeed";
            packetAlert.description = "Vehcile is overspeeding at " + packets.speed + " kmph";
            packetAlert.show = true;
            packetAlert.data = packets.speed;
            packetAlert.category = "Speed";
            break;

        case "F":
            packetAlert.severity = "Normal";
            packetAlert.type = "Motion Start";
            packetAlert.description = "Vehicle started moving again";
            packetAlert.show = false;
            packetAlert.data = "1";
            packetAlert.category = "Motion";
            break;

        case "G":
            packetAlert.severity = "Crucial";
            packetAlert.type = "Motion stop ";
            packetAlert.description = "Vehicle is at halt from past few minutes";
            packetAlert.show = false;
            packetAlert.data = "0";
            packetAlert.category = "Motion";
            break;

        case "H":
            packetAlert.severity = "fatal";
            packetAlert.type = "Gps BreakDown";
            packetAlert.description = "Vehicle unable to connect to Gps satellite from past 2 hours , vehicle might be under some roof";
            packetAlert.show = false;
            packetAlert.data = "0";
            packetAlert.category = "Device";
            break;

        case "I":
            packetAlert.severity = "Normal";
            packetAlert.category = "Geofence";
            if (packets.cmd_key == 1) {
                packetAlert.type = "Entered";
                packetAlert.description = "Entered geofence";
                packetAlert.data = packets.command_value;
            }
            if (packets.cmd_key == 0) {
                packetAlert.type = "Exited";
                packetAlert.description = "Exited Geofence ";
                packetAlert.data = packets.command_value;
            }
            packetAlert.show = true;
            break;



        case "J":
            packetAlert.severity = "Normal";
            packetAlert.category = "Ignition";
            if (packets.ignition == 0) {
                packetAlert.type = "Ignition Off";
                packetAlert.description = "Vehicle has been turned OFF ";
                packetAlert.data = "0";

            }
            if (packets.ignition == 1) {
                packetAlert.type = "Ignition On";
                packetAlert.description = "Vehicle has been turned ON ";
                packetAlert.data = "1";
            }
            packetAlert.show = false;
            break;

        case "K":
            packetAlert.severity = "fatal";
            packetAlert.category = "Device";
            if (packets.box_open_status == 0) {
                packetAlert.type = "Box closed";
                packetAlert.description = "Gps device box is closed";
                packetAlert.data = "0";
            }
            if (packets.box_open_status == 1) {
                packetAlert.type = "Box open";
                packetAlert.description = "Gps device box has been opened ";
                packetAlert.data = "1";
            }
            packetAlert.show = true;
            break;

        case "L":

            packetAlert.category = "Device";
            if (packets.power_status == 0) {
                packetAlert.severity = "fatal";
                packetAlert.type = "Battery disconnected";
                packetAlert.description = "Device working on internal battery";
                packetAlert.data = "0";
            }
            if (packets.power_status == 1) {
                packetAlert.severity = "Normal";
                packetAlert.type = "Battery connected";
                packetAlert.description = "Device running on external battery";
                packetAlert.data = "1";
            }
            packetAlert.show = true;
            break;

        case "M":
            packetAlert.severity = "Normal";
            packetAlert.category = "Messsge";
            packetAlert.type = "Information";
            packetAlert.description = "Message button is pressed by the driver";
            packetAlert.show = true;
            packetAlert.data = "1";
            break;

        case "N":
            packetAlert.severity = "fatal";
            packetAlert.category = "Message";
            packetAlert.type = "SOS";
            packetAlert.description = "SOS from vehicle please contact the driver";
            packetAlert.show = true;
            packetAlert.data = "0";
            break;


        case "O":
            packetAlert.severity = "Normal";
            packetAlert.category = "Device";
            if (packets.digital_io == 0 || packets.digital_io == 241) {
                // TODO: discuss on digital I/O
                packetAlert.type = "Digital_io_1";
                packetAlert.description = "Digital io off ";
                packetAlert.data = "0";

            }
            if (packets.digital_io == 1 || packets.digital_io == 254) {
                // TODO: discuss on digital I/O
                packetAlert.type = "Digital_io_1";
                packetAlert.description = "Digital io on ";
                packetAlert.data = "1";
            }
            packetAlert.show = true;
            break;

        case "Q":
            packetAlert.category = "configuration";

            if (packets.cmd_key == 19) {
                // TODO : discuss
                packetAlert.severity = "Normal";
                packetAlert.type = "Set Speed";
                packetAlert.description = "Speed limit has been set to " + packets.command_value + " kmph successfully";
                // TODO: Discuss on ommand key value  dataArray[5]
                packetAlert.data = packets.command_value;
                // TODO: Discuss on ommand key value  dataArray[5]
            }
            if (packets.cmd_key == 20) {
                // TODO : discuss
                packetAlert.severity = "Crucial";
                packetAlert.type = "Start Vehicle";
                packetAlert.description = "Command to start vehicle has been sent to device successfully";
                packetAlert.data = "1"
            }
            if (packets.cmd_key == 21) {
                // TODO : discuss
                packetAlert.severity = "Crucial";
                packetAlert.type = "Stop Vehicle";
                packetAlert.description = "Command to stop vehicle has been sent to device successfully";
                packetAlert.data = "0";
            }
            packetAlert.show = true;

            break;

        case "S":
            packetAlert.severity = "crucial";
            packetAlert.category = "driving";
            packetAlert.type = "Harsh breaking";
            packetAlert.description = "Driver is breaking harshly there are chances of accident";
            packetAlert.show = false;
            packetAlert.data = "0";
            break;


        case "T":
            packetAlert.severity = "fatal";
            packetAlert.category = "configuration";
            packetAlert.type = "Immobilized";
            packetAlert.description = "Vehicle has been immobilized successfully";
            packetAlert.show = true;
            packetAlert.data = "1";
            break;

        case "U":
            packetAlert.severity = "fatal";
            packetAlert.category = "Device";
            packetAlert.type = "Gps Failed";
            packetAlert.description = "Vehcile is out of Gps coverage area";
            packetAlert.show = true;
            packetAlert.data = "0";
            break;

        case "Z":
            packetAlert.severity = "crucial";
            packetAlert.category = "configuration";
            packetAlert.type = "OTA";
            packetAlert.description = "device has been upgraded succesfully over the air";
            packetAlert.show = false;
            packetAlert.data = "1";
            break;

        default:
            packetAlert = false;
    }

    cb(packetAlert);


}



function createPacketTOSend(packets, cb) {
    var gpspacket = {
        "data": {
            "relationships": {
                "device": {}
            },
            "includes": {
                "alerts": []
            }
        }
    };

    var newDate = packets.timestamp;

    //@TODO implement status packet is online or offline;

    if (packets.power_status == 1) {
        connected = "External";
    } else {
        connected = "Internal";
    }

    gpspacket.data.type = "packet";

    gpspacket.data.attributes = {
        ignition: packets.ignition,
        odometer: packets.odometer,
        speed: packets.speed,
        no_of_satellites: packets.no_of_satellites,
        latitude: packets.latitude,
        longitude: packets.longitude,
        altitude: packets.altitude,
        direction: packets.direction,
        tStamp: newDate,
        fuel: packets.analog_1,
        temperature: packets.analog_2,
        gsm_signal: packets.gsm_signal,
        battery: {
            connected: connected,
            internal: function(con, volt) {
                return volt;
            }(connected, packets.internal_battery),
            external: function(con, volt) {
                return volt;
            }(connected, packets.external_battery),
        },

        // digital_io_2: packets.digital_io_2_status,
        //@TODO  : digital I/O
        // fuel: packets.fuel_in_voltage,
        //@TODO : fuel info
        status: packets.packet_status
    }


    gpspacket.data.relationships.device = {
        serial_number: packets.serial_number,
        model: "UTRACK_1",
        vendor: "SRUSHTI"
    }



    cb(gpspacket);
    //
}



//======================== This getGeofenceAlerts for new schema =====
/* getGeofenceAlerts causing memory leak */
function getGeofenceAlerts(packets, geoFence, cb) {
    var geoFenceAlerts, radius;
    if (geoFence[packets.serial_number].status == false) {
        radius = "";
        longitude = parseFloat(packets.longitude);
        latitude = parseFloat(packets.latitude);
        var packetLoc = {
            latitude: latitude,
            longitude: longitude
        };
        var fence
        db.routes.findOne({
            'vehicles': {
                $elemMatch: {
                    'device.serial_number': packets.serial_number
                }
            }
        }, {
            'locations': 1
        }, function(err, data) {
            console.log('data here ', data);
            if (data) {
                // if data is not null
                if (data.locations.length > 0) {
                    // means routes has locations
                    convertDbLatLngToGeoLibLatLng(data.locations, function(locations) {

                        checkLocInFence(packetLoc, locations, function(location) {
                            if (location === false) {
                                // means no geofance
                                geoFenceAlerts = false;
                                cb(geoFenceAlerts, geoFence);

                            } else {
                                // it is in geofance

                                geoFence[packets.serial_number].status = true;
                                alert = true;
                                geoFence[packets.serial_number].location = location;
                                console.log("locaton", location);
                                gcategory = "Geofence";
                                gseverity = "Normal";
                                gtype = "Enter";
                                gshow = true;
                                gdesc = "Vehicle is in geofence";


                                geoFenceAlerts = {
                                    severity: gseverity,
                                    category: gcategory,
                                    data: geoFence[packets.serial_number].location.loc_id,
                                    type: gtype,
                                    description: gdesc,
                                    show: gshow,
                                    tStamp: packets.timestamp,
                                    latitude: packets.latitude,
                                    longitude: packets.longitude,
                                    serial_number: packets.serial_number
                                }
                                cb(geoFenceAlerts, geoFence);
                            }
                        })
                    });

                } else {
                    // means routes not has locations
                    geoFenceAlerts = false;
                    cb(geoFenceAlerts, geoFence);
                }
            } else {
                // if data is null
                geoFenceAlerts = false;
                cb(geoFenceAlerts, geoFence);
            }
        })
    }

    if (geoFence[packets.serial_number].status == true) {
        console.log("already inside");
        longitude = parseFloat(packets.longitude);
        latitude = parseFloat(packets.latitude);
        var inside;

        // checking for location type
        if (geoFence[packets.serial_number].location.geometry.type === "circle") {
            inside = geolib.isPointInCircle({
                latitude: latitude,
                longitude: longitude
            }, geoFence[packets.serial_number].location.geometry.center, geoFence[packets.serial_number].location.geometry.radius);
        }

        if (geoFence[packets.serial_number].location.geometry.type === "polygon") {
            inside = geolib.isPointInside({
                latitude: latitude,
                longitude: longitude
            }, geoFence[packets.serial_number].location.geometry.coordinates);
        }
        console.log("inside ", inside);
        if (inside) {
            console.log("normal condition if already inside");
            geoFenceAlerts = false;
            cb(geoFenceAlerts, geoFence);

        } else {
            geoFence[packets.serial_number].status = false;
            alert = true;
            gcategory = "Geofence";
            gseverity = "Normal";
            gtype = "Exit";
            gshow = true;
            gdesc = "Vehicle exited geofence";
            geoFenceAlerts = {
                severity: gseverity,
                category: gcategory,
                data: geoFence[packets.serial_number].location.loc_id,
                type: gtype,
                description: gdesc,
                show: gshow,
                tStamp: packets.timestamp,
                latitude: packets.latitude,
                longitude: packets.longitude,
                serial_number: packets.serial_number
            }
            cb(geoFenceAlerts, geoFence);
        }
    }



}

function convertDbLatLngToGeoLibLatLng(locations, cb) {
    // take db locations and convet it into geo lib form
    // IT is full depand on schema
    // [long , lat] => { latitude : lat , longitude : long}
    var locs = locations.map(function(location) {
        if (location.geometry.type === "circle") {
            var loc = {
                latitude: location.geometry.center[1],
                longitude: location.geometry.center[0]
            };
            location.geometry.center = loc;
            return location;
        }
        if (location.geometry.type === "polygon") {
            var locs = location.geometry.coordinates.map(function(loc) {
                return {
                    latitude: loc[1],
                    longitude: loc[0]
                };
            })
            location.geometry.coordinates = locs;
            return location;
        }
    });
    cb(locs);
}

function checkLocInFence(loc, locations, cb) {
    // check via geolib
    var inLocation = false;
    for (i in locations) {
        if (locations[i].geometry.type === "polygon") {
            isInLocation = geolib.isPointInside(loc, locations[i].geometry.coordinates);
            // @TODO : make sure that polygon coordinates in clockwise order
            if (isInLocation) {
                inLocation = locations[i];
                break;
            }

        }
        if (locations[i].geometry.type === "circle") {
            isInLocation = geolib.isPointInCircle(loc, locations[i].geometry.center, locations[i].geometry.radius);

            if (isInLocation) {
                inLocation = locations[i];
                break;
            }
        }
    }
    console.log("In Location ", inLocation);
    cb(inLocation);
}

//====================================================================


//============================Address service=========
function setAddress(packet, cb) {
    locations.address.findOne({
        'location': {
            $near: {
                $geometry: {
                    type: 'Point',
                    coordinates: [parseFloat(packet.data.attributes.longitude), parseFloat(packet.data.attributes.latitude)]
                }
            }
        }
    }, function(err, loc) {
        if (err) {
            packet.data.attributes.address = "see on map";
            if (packet.data.includes.alerts.length > 0) {
                for (var i in packet.data.includes.alerts) {
                    packet.data.includes.alerts[i].address = "see on map";
                }
            }
            cb(packet);
        } else {
            var start = {
                latitude: packet.data.attributes.latitude,
                longitude: packet.data.attributes.longitude
            };
            var end = {
                latitude: loc.location.coordinates[1],
                longitude: loc.location.coordinates[0]
            };
            var distance = (parseFloat(geolib.getDistance(start, end)) / 1000).toFixed(3)
            var address = distance + ' KM from ' + loc.address;
            packet.data.attributes.address = address;
            if (packet.data.includes.alerts.length > 0) {
                for (var i in packet.data.includes.alerts) {
                    packet.data.includes.alerts[i].address = address;
                }
            }
            cb(packet);
        }

    })
}
//====================================================




var timeStampToDate = function(timestamp) {
    var yy = '20' + timestamp.substring(0, 2);
    var mm = parseInt(timestamp.substring(2, 4)) - 1; //moment count month 0-11
    var dd = timestamp.substring(4, 6);
    var HH = timestamp.substring(6, 8);
    var MM = timestamp.substring(8, 10);
    var SS = timestamp.substring(10, 12);

    var date = new Date(moment({
        y: yy,
        M: mm,
        d: dd,
        h: HH,
        m: MM,
        s: SS
    })._d);
    return date;
};

var addMinutes = function(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
};
